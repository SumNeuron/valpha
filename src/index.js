export const identity = (id, field, record) => record[field]
export const lookup = (object, key, fallback) => key in object ? object[key] : fallback
export const defaultFieldRenderFunction = identity
export const defaultFieldSortByFunction = identity
export const defaultFieldFilterFunction = identity
export const isObjectEmpty = (obj) => (Object.entries(obj).length === 0 && obj.constructor === Object)


export const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}


export const uuidv4 = () => {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

export const hasSelectedValidOption = (selected, options) => {
  return Array.isArray(options)
    ? options.indexOf(selected) > -1
    : selected in options;
}


export const hasProvidedValidFilter = (
  filter,
  fields,
  configCNFLogic={},
  configCNFFunctions={},
  configCNFConditionals={}
) => {
  return [
    hasSelectedValidOption(filter.logic, configCNFLogic),
    hasSelectedValidOption(filter.field, fields),
    hasSelectedValidOption(filter.function, configCNFFunctions),
    hasSelectedValidOption(filter.conditional, configCNFConditionals),
    filter.input !== ''
  ].every(x => x === true);
}






/**
 * Group an array of filters such that each sub-array starts with a logical "and" and all other filters are logical "or".
 * @param {array} filters - a list of filter objects
 */
export function groupByConjunctiveNormalForm(filters) {
  let grouped = [];

  // for each filter
  for (let i = 0; i < filters.length; i++) {
    let logic = filters[i].logic;

    // logical "and" marks the beginning of a new sublist
    if (logic === "and") {
      grouped.push([filters[i]]);
      continue
    } else {
      // dealing with a logical "or", will be added to latest grouping
      let list = grouped[grouped.length - 1];

      // somehow, no logical "and" anywhere
      if (typeof list === "undefined") {
        // coerce logical "or" to logical "and"
        filters[i].logic = "and";
        grouped.push([filters[i]]);
      } else {
        grouped[grouped.length - 1].push(filters[i]);
      }
    }
  }
  return grouped;
}

export const getFilterAttributeFromConfig = (filter, prop, config) => {
  // lookup default way of transforming funtions / conditionals as specified in
  // the passed `config`.
  let attr = config[filter[prop]];
  let func = attr[prop];
  if (attr.exceptions && filter.field in attr.exceptions) {
    return attr.exceptions[filter.field]
  }
  return func
}

export const getExtractorFromConfig = (field, config) => {
  if (field in config) {
    return config[field]
  }
  return identity
}

export async function conjunctiveNormalFormFilter(
  json,
  filters=[],
  configLogic={},
  configFunctions={},
  configConditionals={},
  configExtractors={}
) {
  let conjunctiveNormalForm = groupByConjunctiveNormalForm(filters);
  let ids = Object.keys(json);

  let filtered = ids.filter(id => {
    // extract current record
    let record = json[id];
    // calculate the truthiness of all logical "and" statements
    let and = conjunctiveNormalForm.map(groupedOrs => {
      // calculate the truthiness of all logical "or" statements
      let or = groupedOrs.some(filter => {

        // extract the function to apply
        let transform = getFilterAttributeFromConfig(filter, 'function', configFunctions)

        // extract the conditional to test
        let compare = getFilterAttributeFromConfig(filter, 'conditional', configConditionals)

        // get the current record's field to test against
        let extractor = getExtractorFromConfig(filter.field, configExtractors)
        let prop      = extractor(id, filter.field, record);

        // apply the transform to the field
        let value     = transform(prop);

        // get the truthiness of comparison
        let result    =  compare(value, filter.input);
        return result
      });
      return or;
    });
    return and.every(e => e === true);
  });
  return filtered;
}

/**
 * Gives all fields for all records in SQL-like object
 * @param {object} json - an object of {id: record} pairs
 */
export function jsonFields(json) {
  let all = [].concat(
    ...Object.keys(json)
    .map((key) => Object.keys(json[key]))
  );
  let unique = [...new Set(all)].sort();
  return unique;
}

/**
 * Given an SQL-like object and a field, returns the type of value for that field
 * @param {object} json - an object of {id: record} pairs
 * @param field - a field which could be found in record, e.g. record[field]
 */
export function jsonFieldSpy(json, field) {
  let first = Object.keys(json)[0];
  let value = json[first][field];
  return extendedType(value);
}

/**
 * Given an SQL-like object and a list of fields, returns the types for the values of those fields
 * @param {object} json - an object of {id: record} pairs
 * @param {array} fields - an array of fields which can be found in any given record, e.g. record[fields[i]]
 */
export function jsonFieldTypes(json, fields) {
  let types = {};
  fields.forEach(field => {
    types[field] = { type: jsonFieldSpy(json, field) };
  });
  return types;
}

export function reduceJSON(json, keys) {
  return Object.keys(json)
    .filter(key => keys.includes(key))
    .reduce((obj, key) => {
      obj[key] = json[key];
      return obj;
    }, {});
}

/**
 * A slight extension of the vanilla JS types
 * @param variable - any js variable
 */
export function extendedType(variable) {
  // start with JS basics
  let type = typeof variable;

  /*
  number and string are sufficient to know which functions and conditionals
  can be applied to them.
  */
  if (["number", "string"].includes(type)) {
    return type;
  }

  /*
  if not a number or string, it might be an array.
  What the elements are of that array might modify what functions
  can be applied. E.g. an array of numbers allows for min, max, mean,
  etc to be applied. Whereas an array of strings does not.
  */
  if (type != "object") {
    return type;
  }

  /* only dealing with object types at this point */
  if (Array.isArray(variable)) {
    type = "array";
    if (!variable.length) {
      return type;
    }

    let subtypes = variable.map((e) => typeof e);
    let subtype = subtypes[0];
    let allSameQ = subtypes.every(e => e === subtype);

    if (allSameQ) {
      type = `${type}:${subtype};`
    }
    return type;
  }
  return type;
}

/**
 * A simple, dynamic sorting function. If both values are are string, compares
 * strings; if both numeric, compares magnitude, else returns none.
 * @param a - first value
 * @param b - second value
 * @param {boolean} isAscending - whether or not to sort ascending or descending
 */
export function simpleSort(a, b, isAscending) {
  if (typeof a == 'string' && typeof b == 'string') {
    return stringSort(a, b, isAscending)
  }
  else if (typeof a == 'number' && typeof b == 'number') {
    return numberSort(a, b, isAscending)
  }
  else {
    // return numberSort(a, b, isAscending)
    return  //-Infinity
  }
}

/**
 * Comparison function for numerical values.
 * By default JavaScript does _not_ sort numbers numerically.
 * @param a - first value
 * @param b - second value
 * @param {boolean} isAscending - whether or not to sort ascending or descending
 */
export function numberSort(a, b, isAscending) {
  return isAscending
    ? a - b
    : b - a
}


/**
 * Comparison function for string values.
 * Use of the method localeCompare with the following options specified:
 * - lang='en'
 * - sensitivity='base'
 * - ignorePunctuation=true
 * @param a - first value
 * @param b - second value
 * @param {boolean} isAscending - whether or not to sort ascending or descending
 */
export function stringSort(a, b, isAscending) {
  const lang = 'en'
  const opts = { sensitivity: 'base', ignorePunctuation: true }
  return isAscending
    ? a.localeCompare(b, lang, opts)
    : b.localeCompare(a, lang, opts)
}

/**
 * Determines which if given record can be sorted on specified fields
 * @param {object} json - an SQL-like object
 * @param {array} id - the record id under consideration
 * @param {array} fields - the fields on which to sort
 * @param {object} fieldSortByFunctions - an object of {field: function} pairs which modified the value to use when sorting
 * functions of in transform should be defined to take the arguments (id, field, record)
 */
export function isRecordSortable(json, id, fields, fieldSortByFunctions) {
  let anyMissing = fields.some(field => {
    let extractor = (field in fieldSortByFunctions) ? fieldSortByFunctions[field] : identity
    let value = extractor(id, field, json[id])
    return (typeof value  === 'undefined' || value == null)
  })
  return !anyMissing
}


/**
 * Determines which records can be sorted (has sortable values)
 * @param {object} json - an SQL-like object
 * @param {array} ids - a subset of record ids to sort, if not specified, applied to all
 * @param {array} fields - a list of fields which occur in each record
 * @param {object} transform - an object of {field: function} pairs which modified the value to use when sorting
 * functions of in transform should be defined to take the arguments (id, record, field)
 */
export function sortableRecords(json, ids, fields, fieldSortByFunctions) {
  let sortable = [], ignorable = [];

  ids.forEach(id => {
    let isSortable = isRecordSortable(json, id, fields, fieldSortByFunctions)
    if (isSortable) {
      // if all fields have a defined value, this record is sortable
      sortable.push(id)
    } else {
      // at least one field is missing a value, not sortable
      ignorable.push(id)
    }
  })
  return [sortable, ignorable]
}

/**
 * Peforms timsort on an SQL-like object
 * @param {object} json - an SQL-like object
 * @param {array} sort - a list of sort specifications
 * @param {array} ids - a subset of record ids to sort, if not specified, applied to all
 * @param {object} fieldSortByFunctions - an object of {field: function} pairs which modified the value to use when sorting
 * functions of in transform should be defined to take the arguments (id, record, field)
 */
export async function timsort(json, sort, ids=undefined, fieldSortByFunctions={}) {
  let use, sortable, ignorable;
  use = ids === undefined ? Object.keys(json) : [...ids]

  // no sort specified, return filtered in order that it was given
  if (sort.length === 0) {
    return use
  }

  // determine which fields are actually sortable
  [sortable, ignorable] = sortableRecords(json, use, sort.map(s=>s.field), fieldSortByFunctions)

  // setting up the comparison functions for the TimSort
  let comparisons = sort.map(({field, isAscending}) => {
    // each comparsion function takes the two record ids, extracts their values,
    // and then passes it to the simpleSort function
    return (id1, id2) => {
      const extractor = (field in fieldSortByFunctions) ? fieldSortByFunctions[field] : identity
      let a = extractor(id1, field, json[id1])
      let b = extractor(id2, field, json[id2])
      return simpleSort(a, b, isAscending)
    }
  })

  // the actual TimSort
  let sorted = sortable.sort((a, b) => {
    // while there is no difference, keep trying comparisons
    let difference
    for (let i = 0; i < comparisons.length; i++) {
      difference = comparisons[i](a, b);
      if (!difference) continue
      else break
    }
    return difference;
  });

  // return sorted and tack on the unsortable
  return sorted.concat(ignorable)
}


/**
 * Remove punctation from text
 */
export function scrubPunctuation(text, pattern=undefined) {
  if (pattern === undefined) {
    // pattern = /\b[-.,()&$#![\]{}"']+\B|\B[-.,()&$#![\]{}"']+\b/g;
    // pattern = /(-?\d+(?:[.,]\d+)*)|[-.,()&$#![\]{}"']+/g;
    pattern = /(\w+(?:[.,-]\w+)*)|[-.,()&$#![\]{}"']+/g;
  }
  return text.replace(pattern, "$1");
}

/**
 * Evaluates if the string contains only alphabetic characters (a-z/A-Z)
 */
export function isAlphabet(str, andSpaceQ = true) {
  let reg = andSpaceQ ? /^[a-z ]+$/i : /^[a-z]+$/i;
  return reg.test(str);
}

/**
 * global search across all fields for raw text
 * @param {string} text - raw text
 * @param {object} json - SQL-like json
 * @param {array} ids - records in json to search
 * @param {array} fields - an array of fields which occur in any given record in which to search
 * @param {object} transform - an object of {field: function} pairs, where
 * function accepts arguments (id, record, field)
 */
export async function giRegexOnFields(text, json, ids, fields, fieldFilterFunctions={}) {
  let reg = new RegExp(text, "gi")
  let matches = [];

  if (text === "") return ids

  ids.map( id => {
    let record = json[id];
    let fieldJoin = fields.map(field => {
        let extractor = (field in fieldFilterFunctions) ? fieldFilterFunctions[field] : identity
        return extractor(id, field, record);
      }).join("");

    let match = fieldJoin.match(reg);

    if (!(match == null || match.join("") == "")) {
      matches.push(id);
    }
  });

  return matches;
}

/**
 * returns the tokens from map
 * @params {object} map - an object of {token: lookup} pairs
 */
export function tokensFromMap(map) {
  return Object.keys(map);
}

/**
 * Sorts tokens in the order in which to apply them
 * @params {array} tokens - a list of tokens to try and match against.
 */
export function sortTokens(tokens) {
  // copy array with .concat() to not affect order of passed item
  let tkns = tokens.concat()
  tkns.sort(function(a, b) {
    // ASC  -> a.length - b.length
    // DESC -> b.length - a.length
    return b.length - a.length;
  });
  return tkns;
}

export const extractTokensFromMap = (map) => sortTokens(tokensFromMap(map))

/**
 * given two token sets, returns whether or not they are identical
 * @param {array} tokenSet1 - a list of token parts
 * @param {array} tokenSet2 - a list of token parts
 */
export function doTokensSetMatch(tokenSet1, tokenSet2) {
  if (tokenSet1.length !== tokenSet2.length) return false;
  for (let i = tokenSet1.length; i--; ) {
    if (tokenSet1[i] !== tokenSet2[i]) return false;
  }
  return true;
}



/**
 * Search raw text for tokens
 * @param {string} text - raw text
 * @param {array} fields - a list of fields that might appear in the json
 */
export function extractFilterFromTokens( //extractTokens
  text,
  fields,
  configTKNFunctionMap={},
  configTKNConditionalMap={},
  functionTokens=undefined,
  conditionalTokens=undefined,
  pattern=undefined
) {
  // store values with defaults
  let filter = {
    logic: "and",
    function: "identity",
    field: undefined,
    conditional: "eq",
    input: undefined
  };


  // remove leading and trailing whitespace
  text = text.trim();

  // remove all punctuation
  text = scrubPunctuation(text, pattern);

  // "tokenize"
  let textTokensUntouched = text.split(" ");

  // make lowercase to standardize comparisons
  let textTokens = textTokensUntouched.map(x => x.toLowerCase());

  // ASSUMPTION: if logic is specified, it will be the first word.
  let firstWord = textTokens[0].toLowerCase();

  if (firstWord === "or") {
    filter.logic = "or";
    textTokens.splice(0, 1);
    textTokensUntouched.splice(0, 1);
  } else if (firstWord === "and") {
    filter.logic = "and";
    textTokens.splice(0, 1);
    textTokensUntouched.splice(0, 1);
  } else {
    filter.logic = "and";
  }

  if (functionTokens === undefined) functionTokens = extractTokensFromMap(configTKNFunctionMap)
  if (conditionalTokens === undefined) conditionalTokens = extractTokensFromMap(configTKNConditionalMap)

  // ASSUMPTION: default function is the identity function
  let [
    tokensAreEqual,
    currentValue,
    valueIndex,
    tokenIndex
  ] = searchTokensForValues(textTokens, functionTokens);

  if (tokensAreEqual) {
    filter.function = configTKNFunctionMap[functionTokens[valueIndex]];
  }

  [
    tokensAreEqual,
    currentValue,
    valueIndex,
    tokenIndex
  ] = searchTokensForValues(textTokens, conditionalTokens);
  if (tokensAreEqual) {
    filter.conditional = configTKNConditionalMap[conditionalTokens[valueIndex]];
  }
  // everything after the conditional is "input", so need to keep track of index
  let conditionalIndex = tokenIndex;
  let conditionalValue = ((currentValue === undefined) ? '' : currentValue);

  // SEARCH text for which field to be applied to
  [
    tokensAreEqual,
    currentValue,
    valueIndex,
    tokenIndex
  ] = searchTokensForValues(textTokens, fields);
  if (tokensAreEqual) {
    filter.field = fields[valueIndex];
  }

  // everything after the conditional is "input"
  filter.input = textTokensUntouched
    .slice(
      conditionalIndex + conditionalValue.split(" ").length,
      textTokens.length
    )
    .join(" ");

  return filter;
}


/**
 * searchs tokens for matching values
 * @param {array} tokens - an array of tokens
 * @param {array} values - an array of values from which to search for tokens
 */
export function searchTokensForValues(tokens, values) {
  let valueIndex = 0;
  let tokenIndex = 0;
  let currentValue;
  let tokenizedValue;
  let tokensAreEqual = false;
  let consecutiveTokens;

  // for each value
  for (valueIndex = 0; valueIndex < values.length; valueIndex++) {
    // current value to match in tokens
    currentValue = values[valueIndex].toLowerCase();

    // "tokenize" current, in case of multiples word are in the value
    tokenizedValue = currentValue.split(" ");

    // look at n consecutive tokens where n is the number of tokenized values
    if (tokenizedValue.length > tokens.length) continue; // too long

    for (tokenIndex = 0; tokenIndex < tokens.length; tokenIndex++) {
      consecutiveTokens = tokens.slice(tokenIndex, tokenIndex + tokenizedValue.length);
      tokensAreEqual = doTokensSetMatch(tokenizedValue, consecutiveTokens);
      if (tokensAreEqual) break; // early exit
    }
    if (tokensAreEqual) break; // early exit
  }
  return [tokensAreEqual, values[valueIndex], valueIndex, tokenIndex];
}


export const findLogicalStatements = (text) => {
  let words = text.split(' ')
  let statementIndicies = []
  for (let i = 0; i < words.length; i++) {
    let word = words[i]
    let isAnd = word.localeCompare('and', undefined, { sensitivity: 'base' })
    let isOr = word.localeCompare('or', undefined, { sensitivity: 'base' })
    let isEqual = false
    if (i < words.length -1) {
      isEqual = words[i+1].localeCompare('equal', undefined, { sensitivity: 'base' })
    }

    if (isAnd === 0 || (isOr === 0 && isEqual !== 0)) {
      statementIndicies.push(i)
    }
  }
  return statementIndicies
}

export const parseTextIntoStatements = (text) => {
  let statementStarts = findLogicalStatements(text)
  if (statementStarts.length === 0) return [text]
  let statements = []
  let words = text.split(' ')
  statements.push(words.slice(0, statementStarts[0]).join(' '))
  for (let i = 0; i < statementStarts.length; i++) {
    let j = statementStarts[i]
    if (i === statementStarts.length-1) {
      statements.push(words.slice(j).join(' '))
    }
    else  {
      statements.push(words.slice(j, statementStarts[i+1]).join(' '))
    }
  }
  if (statements[0] === '') return statements.slice(1)
  return statements
}


export const extractFiltersFromText = async (
  text, fieldTokens,
  configCNFLogic={},
  configCNFFunctions={},
  configCNFConditionals={},
  configTKNFunctionMap={},
  configTKNConditionalMap={},
  functionTokens=undefined,
  conditionalTokens=undefined,
  pattern=undefined,
  humanFriendlyReverseLookup={}
) => {
  if (functionTokens === undefined) functionTokens = extractTokensFromMap(configTKNFunctionMap)
  if (conditionalTokens === undefined) conditionalTokens = extractTokensFromMap(configTKNConditionalMap)

  // break text into chunks of logical statments
  let statements = parseTextIntoStatements(text)
  let isValid = [], isError = []
  statements.forEach((statement)=>{
    let filter = extractFilterFromTokens(
      statement,
      fieldTokens,
      configTKNFunctionMap,
      configTKNConditionalMap,
      functionTokens,
      conditionalTokens,
      pattern
    )

    let isValidFilter = hasProvidedValidFilter(
      filter,
      fieldTokens,
      configCNFLogic,
      configCNFFunctions,
      configCNFConditionals,
    )

    let internalField = humanFriendlyReverseLookup[filter.field]
    if (internalField !== undefined) {
      filter.field = internalField
    }

    if (isValidFilter) {
      isValid.push({filter, text:statement})
    } else {
      isError.push({filter, text:statement})
    }
  })
  return [isValid, isError]
}


export const sortDirectionFromSpecification = (sortSpecifications, field) => {
  for (let i = 0; i < sortSpecifications.length; i++) {
    if (sortSpecifications[i].field === field) {
      return sortSpecifications[i].isAscending
    }
  }
  return undefined
}

export const sortNumberFromSpecification = (sortSpecifications, field) => {
  for (let i = 0; i < sortSpecifications.length; i++) {
    if (sortSpecifications[i].field === field) {
      return i
    }
  }
  return undefined
}


export const toggleSortSpecification = (sortSpecifications, field) => {
  let sortSpecs = sortSpecifications
  let sortSpec = {field: field, isAscending: true}

  let found = false
  let i
  for (i = 0; i < sortSpecs.length; i++) {
    let cur = sortSpecs[i]
    if (cur.field === field) {
      if (cur.isAscending) {
        sortSpec.isAscending = false
        sortSpecs[i] = sortSpec
        found = true
        break
      } else {
        sortSpecs.splice(i, 1)
        found = true
        break
      }
    }
  }
  if (!found) {
    sortSpecs.push(sortSpec)
  }
  return sortSpecs
}


export function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		clearTimeout(timeout);
		timeout = setTimeout(function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		}, wait);
		if (immediate && !timeout) func.apply(context, args);
	};
}
