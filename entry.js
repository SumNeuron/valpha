import Vue from 'vue';

import VFilterView from './components/VFilterView.vue'
import VTimSort from './components/VTimSort.vue'
import VAlphaToggle from './components/VAlphaToggle.vue'
import VRecordsTable from './components/VRecordsTable.vue'
import VConfig from './components/VConfig.vue'

import * as valpha from './src/index.js'
import * as config from './src/config.js'

const components = {
  VFilterView,
  VTimSort,
  VAlphaToggle,
  VRecordsTable,
  VConfig
}



function install(Vue) {
  if (install.installed) return;
  install.installed = true;

  Object.keys(components).forEach(name => {
    Vue.component(name, components[name])
  });

}

const plugin = {
  install,
}

let GlobalVue = null;
if (typeof window !== 'undefined') {
  GlobalVue = window.Vue;
} else if (typeof global !== 'undefined') {
  GlobalVue = global.Vue;
}
if (GlobalVue) {
  GlobalVue.use(plugin);
}


export default components
export const strict = false

export {
  VFilterView,
  VTimSort,
  valpha,
  config,
  VAlphaToggle,
  VRecordsTable,
  VConfig
}
